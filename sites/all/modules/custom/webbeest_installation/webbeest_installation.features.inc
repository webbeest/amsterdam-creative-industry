<?php
/**
 * @file
 * d_media_installation.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function webbeest_installation_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
